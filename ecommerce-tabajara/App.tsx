import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import UserLayout from './layout/UserLayout';
import PersonList from './views/person/PersonList';
import PersonDetails from './views/person/PersonDetails';
import PersonForm from './views/person/PersonForm';
import EnderecoForm from './views/endereco/EnderecoForm';

import {Provider} from "react-redux";
import store from "./redux/store"


const Stack = createStackNavigator();


export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="UserLayout">
          
          <Stack.Screen name="UserLayout" component={UserLayout}  />
          <Stack.Screen name="PersonList" component={PersonList} />
          <Stack.Screen name="PersonDetails" component={PersonDetails} />
          <Stack.Screen name="PersonForm" component={PersonForm} />
          <Stack.Screen name="EnderecoForm" component={EnderecoForm} />

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
