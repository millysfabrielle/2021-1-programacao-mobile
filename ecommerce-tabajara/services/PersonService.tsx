import axios from 'axios'; 

class PersonService{

    connection = axios.create({baseURL: 'http://localhost:3000/'});


    findAll(){
        return this.connection.get('/persons');
    }

    findById( id ){
        return this.connection.get('/persons/'+id);
    }

    save( person ){

        if(person.id == undefined){
            return this.connection.post('/persons/', person)
        } 
        else {
            return this.connection.put('/persons/'+person.id, person)
        }

    }

    delete(id){
        return this.connection.delete('/persons/'+id);
    }

}

export default new PersonService();