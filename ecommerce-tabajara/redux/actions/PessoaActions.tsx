import PersonService from "../../services/PersonService";

export const PESSOA_ACTIONS = {
    LISTAR: "PESSOA_LISTAR",
    BUSCAR: "PESSOA_BUSCAR",
    SALVAR: "PESSOA_SALVAR",
    ALTERAR: "PESSOA_ALTERAR",
    EXCLUIR: "PESSOA_EXCLUIR",
    CLEAR_ITEM: "PESSOA_CLEAR_ITEM",
}

export function clearPessoaItem(){
    return function(callback){
        callback({
            type: PESSOA_ACTIONS.CLEAR_ITEM,
            content: null
        });
    }
}

export function listarPessoa(){
    return function(callback){
        PersonService.findAll()
            .then( response => {

                callback({
                    type: PESSOA_ACTIONS.LISTAR,
                    content: response.data
                });

            })
            .catch( error => console.log(error))
    }
}

export function buscarPessoaId(id){
    return function(callback){
        PersonService.findById(id)
            .then( response => {
                callback({
                    type: PESSOA_ACTIONS.BUSCAR,
                    content: response.data
                })
            })
            .catch( error => { console.log(error) })
    }
}

export function excluirPessoa(id){
    return function(callback){
        PersonService.delete(id)
            .then( response => {
                callback({
                    type: PESSOA_ACTIONS.EXCLUIR,
                    content: id
                })
            })
            .catch( error => { console.log(error) })
    }
}

export function salvarPessoa(pessoa){
    return function(callback){

        const action = pessoa.id != undefined ? PESSOA_ACTIONS.ALTERAR : PESSOA_ACTIONS.SALVAR;

        PersonService.save(pessoa)
            .then( response => {
                callback({
                    type: action,
                    content: response.data
                })
            })
            .catch( error => { console.log(error) })


    }
}

