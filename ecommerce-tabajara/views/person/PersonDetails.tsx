import React from 'react';

// Componentes
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, Alert, Pressable } from 'react-native';
import Modal from 'modal-react-native-web'; // apenas para DEV
import { Button, Overlay, Icon  } from 'react-native-elements';

import Hr from "../../components/Hr";

// redux
import { connect } from "react-redux";
import {buscarPessoaId, excluirPessoa} from "../../redux/actions/PessoaActions";
import {pessoaFilter} from "../../redux/filters/PessoaFilter";

class PersonDetails extends React.Component{
    
    constructor(props){
        super(props);

        const {id} = this.props.route.params;
        this.state = { 
            id: id,   
            modalVisible: false
        }

        this.props.navigation.setOptions({
            title: "Detalhes da pessoa",
            headerRight: () => (
                <View style={styles.flexContainer} >

                    <Button
                        type="clear"
                        icon={ <Icon name='refresh-circle-outline' size={30} type='ionicon' color='#333333' /> }
                        onPress={ () => this.onPressUpdate() }
                    />

                    <Button
                        type="clear"
                        icon={ <Icon name='trash-outline' type='ionicon' size={30} color='#333333' /> }
                        onPress={ () => this.setModalVisible() }
                    />

                </View>
            ),

        });

    }

    onPressUpdate = () => {
        this.props.navigation.navigate('PersonForm',{id: this.state.id });
    }
    onPressExcluir = () => {

        this.props.excluirPessoa(this.state.id);
        this.setModalVisible();
        this.props.navigation.navigate('PersonList');
        
    }
 
    setModalVisible = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    }


    componentDidMount(){
        this.props.buscarPessoaId(this.state.id);
    }

    render(){

        return(
            <View style={styles.container}>
                
                <View style={styles.flexContainer} >
                    <Image style={styles.userIcon} source={{ uri: this.props.pessoaItem.img }} />
                    <Text style={styles.userName} >{this.props.pessoaItem.name}</Text>
                </View>

                <View style={[styles.flexContainer, styles.textInfo]}>
                    <Text style={styles.textBold}> CPF: </Text>
                    <Text> {this.props.pessoaItem.cpf}  </Text>
                </View>

                <Hr text="Endereços" />

                <View style={styles.enderecoContainer}>
                    <Text style={styles.enderecoText}> Rua Copacabana </Text>
                    <Text style={styles.enderecoText}> Casa 1 </Text>
                    <Text style={styles.enderecoText}> 74500-400 </Text>
                </View>

                <Button
                    style= {styles.flexAlignEnd}
                    type="clear"
                    icon={ <Icon name='add-circle-outline' type='ionicon' size={30} color='#333333' /> }
                    onPress={ () => this.props.navigation.navigate('EnderecoForm',{id: this.state.id }) }
                />


                <Overlay ModalComponent={Modal} isVisible={this.state.modalVisible} onBackdropPress={() => this.setModalVisible()}>
                    <Text style={styles.textHeader}>Confirmação !</Text>
                    <Text>Deseja excluir o {this.props.pessoaItem.name} ?</Text>

                    <View  style={styles.container}>
                        <Button
                            type="solid"
                            onPress={ () => this.setModalVisible() }
                            title="Cancelar"
                            buttonStyle={styles.btnSecondary}
                        />
                        <Button
                            type="solid"
                            onPress={ () => this.onPressExcluir() }
                            title="Excluir"
                            buttonStyle={styles.btnDanger}
                        />
                    </View>

                </Overlay>


            </View>
        )
    }

}

//export default PersonDetails;
export default connect(pessoaFilter, {buscarPessoaId, excluirPessoa})(PersonDetails)

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    flexContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
    },
    flexAlignEnd: {
        alignItems: "flex-end"
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        borderBottomColor: "#000",
        borderBottomWidth: 1,
        margin: 10
    },
    textBold: {
        fontWeight: "bold"
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    },

    enderecoContainer:{
        backgroundColor: "#e0e0e0"
    },
    enderecoText:{
        fontSize: 14
    }

   

});