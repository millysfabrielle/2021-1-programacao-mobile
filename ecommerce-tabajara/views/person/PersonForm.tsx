import React from 'react';

// Componentes
import { FlatList, SafeAreaView, View, StatusBar, StyleSheet, Text, TextInput } from "react-native";
import { Input, Button } from 'react-native-elements';

// Formik
import { Formik } from 'formik';

// Redux
import { connect } from "react-redux";
import {salvarPessoa,clearPessoaItem} from "../../redux/actions/PessoaActions";
import {pessoaFilter} from "../../redux/filters/PessoaFilter";

class PersonForm extends React.Component{

    constructor(props){
        super(props);

        const {id} = this.props.route.params;
        if(id == undefined){
            this.props.clearPessoaItem();
        }

        
    }

    componentDidMount(){}

    handleSubmit = (pessoa) => {
        this.props.salvarPessoa(pessoa);
        this.props.navigation.navigate('PersonList');
    }

    mapInitialValues = () => {
        return {
            id: this.props.pessoaItem.id || undefined,
            name: this.props.pessoaItem.name || '',
            cpf: this.props.pessoaItem.cpf || '',
            img: this.props.pessoaItem.img || ''
            
        };
    }

    render(){
        return(
            <SafeAreaView>
                
                <Text> Cadastro de pessoa.</Text>
                
                <Formik
                    initialValues={ this.mapInitialValues()}
                    onSubmit={ (values) => this.handleSubmit(values) }
                    enableReinitialize
                >

                    { ({handleChange, handleSubmit,values}) => (

                        <View>

                            <Input label="Nome" placeholder='Nome' value={values.name} onChangeText={handleChange("name")} />
                            <Input label="CPF" placeholder='CPF' value={values.cpf} onChangeText={handleChange("cpf")} />

                            <Button title="Salvar" onPress={handleSubmit} />

                        </View>

                    )}

                </Formik>

                        



            </SafeAreaView>
        )
    }

}

export default connect(pessoaFilter, {salvarPessoa,clearPessoaItem})(PersonForm)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        borderBottomColor: "#000",
        borderBottomWidth: 1,
        margin: 10
    },
    textBold: {
        fontWeight: "bold"
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    }

   

});