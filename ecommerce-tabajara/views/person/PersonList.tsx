import React from 'react';

// Componentes
import { FlatList, SafeAreaView, View, StatusBar, StyleSheet, Text, TouchableOpacity } from "react-native";
import { Button, Overlay, Icon  } from 'react-native-elements';
import UserListComponent from "../../components/UserListComponent";

// Redux
import { connect } from "react-redux";
import {listarPessoa} from "../../redux/actions/PessoaActions";
import {pessoaFilter} from "../../redux/filters/PessoaFilter";


class PersonList extends React.Component{
    constructor(props){
        super(props);

        this.props.navigation.setOptions({
            title: "Lista de pessoas",
            headerRight: () => (
                <View style={styles.container} >

                    <Button
                        type="clear"
                        icon={ <Icon name='add-circle-outline' type='ionicon' size={30} color='#333333' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('PersonForm',{id: undefined });
    }

    componentDidMount(){
        this.props.listarPessoa();
        console.log("LISTA = ", this.props.pessoaLista);
    }

    render(){
        return(
            <SafeAreaView>
                <FlatList
                    data={this.props.pessoaLista}
                    renderItem={UserListComponent}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default connect(pessoaFilter,{listarPessoa})(PersonList);

const styles = StyleSheet.create({});